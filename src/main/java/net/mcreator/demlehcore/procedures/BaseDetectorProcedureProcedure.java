package net.mcreator.demlehcore.procedures;

import net.minecraft.world.IWorld;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.Entity;
import net.minecraft.block.Blocks;

import net.mcreator.demlehcore.block.SouffreChestBlock;
import net.mcreator.demlehcore.block.MiniumChestBlock;
import net.mcreator.demlehcore.block.EboniteChestBlock;
import net.mcreator.demlehcore.block.AmazoniteChestBlock;
import net.mcreator.demlehcore.DemlehcoreMod;

import java.util.Random;
import java.util.Map;

public class BaseDetectorProcedureProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency world for procedure BaseDetectorProcedure!");
			return;
		}
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency entity for procedure BaseDetectorProcedure!");
			return;
		}
		if (dependencies.get("itemstack") == null) {
			if (!dependencies.containsKey("itemstack"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency itemstack for procedure BaseDetectorProcedure!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		Entity entity = (Entity) dependencies.get("entity");
		ItemStack itemstack = (ItemStack) dependencies.get("itemstack");
		boolean logY = false;
		boolean detect_coffre = false;
		double coordX = 0;
		double coordY = 0;
		double coordZ = 0;
		double zz = 0;
		double yy = 0;
		double xx = 0;
		double text_z = 0;
		double test_x = 0;
		double NumCoffre = 0;
		{
			ItemStack _ist = itemstack;
			if (_ist.attemptDamageItem((int) 1, new Random(), null)) {
				_ist.shrink(1);
				_ist.setDamage(0);
			}
		}
		if (Math.floor(entity.getPosX()) % 16 == 0 || Math.floor(entity.getPosZ()) % 16 == 0 || Math.floor(entity.getPosX() + 1) % 16 == 0
				|| Math.floor(entity.getPosZ() + 1) % 16 == 0) {
			NumCoffre = 0;
			test_x = (Math.floor(entity.getPosX() / 16) * 16);
			text_z = (Math.floor(entity.getPosZ() / 16) * 16);
			zz = text_z;
			detect_coffre = (false);
			for (int index0 = 0; index0 < (int) (40); index0++) {
				yy = 2;
				for (int index1 = 0; index1 < (int) (125); index1++) {
					xx = test_x;
					for (int index2 = 0; index2 < (int) (40); index2++) {
						if ((world.getBlockState(new BlockPos(xx, yy, zz))).getBlock() == Blocks.CHEST
								|| (world.getBlockState(new BlockPos(xx, yy, zz))).getBlock() == Blocks.CRAFTING_TABLE
								|| (world.getBlockState(new BlockPos(xx, yy, zz))).getBlock() == AmazoniteChestBlock.block
								|| (world.getBlockState(new BlockPos(xx, yy, zz))).getBlock() == SouffreChestBlock.block
								|| (world.getBlockState(new BlockPos(xx, yy, zz))).getBlock() == MiniumChestBlock.block
								|| (world.getBlockState(new BlockPos(xx, yy, zz))).getBlock() == EboniteChestBlock.block) {
							detect_coffre = (true);
							NumCoffre = (NumCoffre + 1);
						}
						xx = (xx + 1);
					}
					yy = (yy + 1);
				}
				zz = (zz + 1);
			}
			if (detect_coffre == true) {
				if (entity instanceof PlayerEntity && !entity.world.isRemote()) {
					((PlayerEntity) entity).sendStatusMessage(
							new StringTextComponent(("Potentielle base trouv\u00E9e! Nombre d'objets suspects: " + NumCoffre)), (true));
				}
			} else {
				if (entity instanceof PlayerEntity && !entity.world.isRemote()) {
					((PlayerEntity) entity).sendStatusMessage(new StringTextComponent("Recherche de base..."), (true));
				}
			}
		}
	}
}
