package net.mcreator.demlehcore.procedures;

import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.Hand;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.demlehcore.item.Fragment4Item;
import net.mcreator.demlehcore.DemlehcoreMod;

import java.util.Map;

public class FauxInterditeProcedure3Procedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency entity for procedure FauxInterditeProcedure3!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency sourceentity for procedure FauxInterditeProcedure3!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHealth() : -1) > 7) {
			if (sourceentity instanceof LivingEntity)
				((LivingEntity) sourceentity).setHealth((float) 7);
		}
		if (entity instanceof LivingEntity)
			((LivingEntity) entity).setHealth((float) 5);
		if (sourceentity instanceof PlayerEntity) {
			ItemStack _stktoremove = ((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY);
			((PlayerEntity) sourceentity).inventory.func_234564_a_(p -> _stktoremove.getItem() == p.getItem(), (int) 1,
					((PlayerEntity) sourceentity).container.func_234641_j_());
		}
		if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemOffhand() : ItemStack.EMPTY)
				.getItem() == (ItemStack.EMPTY).getItem()) {
			if (sourceentity instanceof LivingEntity) {
				ItemStack _setstack = new ItemStack(Fragment4Item.block);
				_setstack.setCount((int) 2);
				((LivingEntity) sourceentity).setHeldItem(Hand.OFF_HAND, _setstack);
				if (sourceentity instanceof ServerPlayerEntity)
					((ServerPlayerEntity) sourceentity).inventory.markDirty();
			}
		} else {
			if (entity instanceof PlayerEntity && !entity.world.isRemote()) {
				((PlayerEntity) entity).sendStatusMessage(new StringTextComponent(
						"Malheureusement, l'utilisation de la faux avec un item dans la main gauche fais en sorte que tu ne re\uFFFDois pas un item n\uFFFDcessaire. Pour l'instant, va voir dash, mais pour le futur, ne mets pas d'item dans ta main gauche en utilisant la faux!"),
						(false));
			}
		}
	}
}
