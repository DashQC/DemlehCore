package net.mcreator.demlehcore.procedures;

import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.World;
import net.minecraft.world.IWorld;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemStack;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;
import net.minecraft.block.Blocks;

import net.mcreator.demlehcore.item.EboniteArmorItem;
import net.mcreator.demlehcore.DemlehcoreMod;

import java.util.Map;
import java.util.Collections;

public class IgnisIraeProcedure2Procedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency world for procedure IgnisIraeProcedure2!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency x for procedure IgnisIraeProcedure2!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency y for procedure IgnisIraeProcedure2!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency z for procedure IgnisIraeProcedure2!");
			return;
		}
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency entity for procedure IgnisIraeProcedure2!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency sourceentity for procedure IgnisIraeProcedure2!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if ((world.getBlockState(new BlockPos(x, y + 5, z))).getBlock() == Blocks.AIR
				&& (world.getBlockState(new BlockPos(x, y + 6, z))).getBlock() == Blocks.AIR
				&& (world.getBlockState(new BlockPos(x, y + 7, z))).getBlock() == Blocks.AIR
				&& (world.getBlockState(new BlockPos(x, y + 8, z))).getBlock() == Blocks.AIR
				&& (world.getBlockState(new BlockPos(x, y + 9, z))).getBlock() == Blocks.AIR
				&& (world.getBlockState(new BlockPos(x, y + 10, z))).getBlock() == Blocks.AIR
				&& (world.getBlockState(new BlockPos(x, y + 11, z))).getBlock() == Blocks.AIR
				&& (world.getBlockState(new BlockPos(x, y + 12, z))).getBlock() == Blocks.AIR) {
			if (((entity instanceof LivingEntity) ? ((LivingEntity) entity).getItemStackFromSlot(EquipmentSlotType.HEAD) : ItemStack.EMPTY)
					.getItem() == EboniteArmorItem.helmet
					&& ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getItemStackFromSlot(EquipmentSlotType.CHEST) : ItemStack.EMPTY)
							.getItem() == EboniteArmorItem.body
					&& ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getItemStackFromSlot(EquipmentSlotType.LEGS) : ItemStack.EMPTY)
							.getItem() == EboniteArmorItem.legs
					&& ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getItemStackFromSlot(EquipmentSlotType.FEET) : ItemStack.EMPTY)
							.getItem() == EboniteArmorItem.boots) {
				world.setBlockState(new BlockPos(x, y + 8, z), Blocks.BEDROCK.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x, y + 9, z), Blocks.LAVA.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x, y + 11, z), Blocks.BEDROCK.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x + 1, y + 9, z), Blocks.BEDROCK.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x - 1, y + 9, z), Blocks.BEDROCK.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x, y + 9, z + 1), Blocks.BEDROCK.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x, y + 9, z - 1), Blocks.BEDROCK.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x + 1, y + 10, z), Blocks.GLASS.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x - 1, y + 10, z), Blocks.GLASS.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x, y + 10, z + 1), Blocks.GLASS.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x, y + 10, z - 1), Blocks.GLASS.getDefaultState(), 3);
				if (entity.getPosX() > 0 && entity.getPosZ() > 0) {
					{
						Entity _ent = entity;
						_ent.setPositionAndUpdate((Math.floor(x) + 0.5), (y + 9), (Math.floor(z) + 0.5));
						if (_ent instanceof ServerPlayerEntity) {
							((ServerPlayerEntity) _ent).connection.setPlayerLocation((Math.floor(x) + 0.5), (y + 9), (Math.floor(z) + 0.5),
									_ent.rotationYaw, _ent.rotationPitch, Collections.emptySet());
						}
					}
				}
				if (entity.getPosX() < 0 && entity.getPosZ() < 0) {
					{
						Entity _ent = entity;
						_ent.setPositionAndUpdate((Math.floor(x) + 0.5), (y + 9), (Math.floor(z) + 0.5));
						if (_ent instanceof ServerPlayerEntity) {
							((ServerPlayerEntity) _ent).connection.setPlayerLocation((Math.floor(x) + 0.5), (y + 9), (Math.floor(z) + 0.5),
									_ent.rotationYaw, _ent.rotationPitch, Collections.emptySet());
						}
					}
				}
				if (entity.getPosX() > 0 && entity.getPosZ() < 0) {
					{
						Entity _ent = entity;
						_ent.setPositionAndUpdate((Math.floor(x) + 0.5), (y + 9), (Math.floor(z) + 0.5));
						if (_ent instanceof ServerPlayerEntity) {
							((ServerPlayerEntity) _ent).connection.setPlayerLocation((Math.floor(x) + 0.5), (y + 9), (Math.floor(z) + 0.5),
									_ent.rotationYaw, _ent.rotationPitch, Collections.emptySet());
						}
					}
				}
				if (entity.getPosX() < 0 && entity.getPosZ() > 0) {
					{
						Entity _ent = entity;
						_ent.setPositionAndUpdate((Math.floor(x) + 0.5), (y + 9), (Math.floor(z) + 0.5));
						if (_ent instanceof ServerPlayerEntity) {
							((ServerPlayerEntity) _ent).connection.setPlayerLocation((Math.floor(x) + 0.5), (y + 9), (Math.floor(z) + 0.5),
									_ent.rotationYaw, _ent.rotationPitch, Collections.emptySet());
						}
					}
				}
				if (world instanceof World && !world.isRemote()) {
					((World) world).playSound(null, new BlockPos(x, y, z),
							(net.minecraft.util.SoundEvent) ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation("entity.ender_dragon.growl")),
							SoundCategory.NEUTRAL, (float) 1, (float) 1);
				} else {
					((World) world).playSound(x, y, z,
							(net.minecraft.util.SoundEvent) ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation("entity.ender_dragon.growl")),
							SoundCategory.NEUTRAL, (float) 1, (float) 1, false);
				}
				new Object() {
					private int ticks = 0;
					private float waitTicks;
					private IWorld world;

					public void start(IWorld world, int waitTicks) {
						this.waitTicks = waitTicks;
						MinecraftForge.EVENT_BUS.register(this);
						this.world = world;
					}

					@SubscribeEvent
					public void tick(TickEvent.ServerTickEvent event) {
						if (event.phase == TickEvent.Phase.END) {
							this.ticks += 1;
							if (this.ticks >= this.waitTicks)
								run();
						}
					}

					private void run() {
						world.setBlockState(new BlockPos(x, y + 8, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x, y + 9, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x, y + 11, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x + 1, y + 9, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x - 1, y + 9, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x, y + 9, z + 1), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x, y + 9, z - 1), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x + 1, y + 10, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x - 1, y + 10, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x, y + 10, z + 1), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x, y + 10, z - 1), Blocks.AIR.getDefaultState(), 3);
						MinecraftForge.EVENT_BUS.unregister(this);
					}
				}.start(world, (int) 100);
			} else {
				world.setBlockState(new BlockPos(x, y + 5, z), Blocks.BEDROCK.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x, y + 6, z), Blocks.LAVA.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x, y + 8, z), Blocks.BEDROCK.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x + 1, y + 6, z), Blocks.BEDROCK.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x - 1, y + 6, z), Blocks.BEDROCK.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x, y + 6, z + 1), Blocks.BEDROCK.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x, y + 6, z - 1), Blocks.BEDROCK.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x + 1, y + 7, z), Blocks.GLASS.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x - 1, y + 7, z), Blocks.GLASS.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x, y + 7, z + 1), Blocks.GLASS.getDefaultState(), 3);
				world.setBlockState(new BlockPos(x, y + 7, z - 1), Blocks.GLASS.getDefaultState(), 3);
				if (entity.getPosX() > 0 && entity.getPosZ() > 0) {
					{
						Entity _ent = entity;
						_ent.setPositionAndUpdate((Math.floor(x) + 0.5), (y + 6), (Math.floor(z) + 0.5));
						if (_ent instanceof ServerPlayerEntity) {
							((ServerPlayerEntity) _ent).connection.setPlayerLocation((Math.floor(x) + 0.5), (y + 6), (Math.floor(z) + 0.5),
									_ent.rotationYaw, _ent.rotationPitch, Collections.emptySet());
						}
					}
				}
				if (entity.getPosX() < 0 && entity.getPosZ() < 0) {
					{
						Entity _ent = entity;
						_ent.setPositionAndUpdate((Math.floor(x) + 0.5), (y + 6), (Math.floor(z) + 0.5));
						if (_ent instanceof ServerPlayerEntity) {
							((ServerPlayerEntity) _ent).connection.setPlayerLocation((Math.floor(x) + 0.5), (y + 6), (Math.floor(z) + 0.5),
									_ent.rotationYaw, _ent.rotationPitch, Collections.emptySet());
						}
					}
				}
				if (entity.getPosX() > 0 && entity.getPosZ() < 0) {
					{
						Entity _ent = entity;
						_ent.setPositionAndUpdate((Math.floor(x) + 0.5), (y + 6), (Math.floor(z) + 0.5));
						if (_ent instanceof ServerPlayerEntity) {
							((ServerPlayerEntity) _ent).connection.setPlayerLocation((Math.floor(x) + 0.5), (y + 6), (Math.floor(z) + 0.5),
									_ent.rotationYaw, _ent.rotationPitch, Collections.emptySet());
						}
					}
				}
				if (entity.getPosX() < 0 && entity.getPosZ() > 0) {
					{
						Entity _ent = entity;
						_ent.setPositionAndUpdate((Math.floor(x) + 0.5), (y + 6), (Math.floor(z) + 0.5));
						if (_ent instanceof ServerPlayerEntity) {
							((ServerPlayerEntity) _ent).connection.setPlayerLocation((Math.floor(x) + 0.5), (y + 6), (Math.floor(z) + 0.5),
									_ent.rotationYaw, _ent.rotationPitch, Collections.emptySet());
						}
					}
				}
				if (world instanceof World && !world.isRemote()) {
					((World) world).playSound(null, new BlockPos(x, y, z),
							(net.minecraft.util.SoundEvent) ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation("entity.ender_dragon.growl")),
							SoundCategory.NEUTRAL, (float) 1, (float) 1);
				} else {
					((World) world).playSound(x, y, z,
							(net.minecraft.util.SoundEvent) ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation("entity.ender_dragon.growl")),
							SoundCategory.NEUTRAL, (float) 1, (float) 1, false);
				}
				new Object() {
					private int ticks = 0;
					private float waitTicks;
					private IWorld world;

					public void start(IWorld world, int waitTicks) {
						this.waitTicks = waitTicks;
						MinecraftForge.EVENT_BUS.register(this);
						this.world = world;
					}

					@SubscribeEvent
					public void tick(TickEvent.ServerTickEvent event) {
						if (event.phase == TickEvent.Phase.END) {
							this.ticks += 1;
							if (this.ticks >= this.waitTicks)
								run();
						}
					}

					private void run() {
						world.setBlockState(new BlockPos(x, y + 5, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x, y + 6, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x, y + 8, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x + 1, y + 6, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x - 1, y + 6, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x, y + 6, z + 1), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x, y + 6, z - 1), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x + 1, y + 7, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x - 1, y + 7, z), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x, y + 7, z + 1), Blocks.AIR.getDefaultState(), 3);
						world.setBlockState(new BlockPos(x, y + 7, z - 1), Blocks.AIR.getDefaultState(), 3);
						MinecraftForge.EVENT_BUS.unregister(this);
					}
				}.start(world, (int) 100);
			}
		} else {
			if (sourceentity instanceof PlayerEntity && !sourceentity.world.isRemote()) {
				((PlayerEntity) sourceentity).sendStatusMessage(
						new StringTextComponent("L'ennemi ne peut pas \u00EAtre emprisonn\u00E9; pr\u00E9sence de blocs juste au dessus"), (false));
			}
		}
	}
}
