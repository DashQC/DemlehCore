package net.mcreator.demlehcore.procedures;

import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.demlehcore.DemlehcoreMod;

import java.util.Map;

public class FauxInterditeProcedure5Procedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency entity for procedure FauxInterditeProcedure5!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency sourceentity for procedure FauxInterditeProcedure5!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if (((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHealth() : -1) > 7) {
			if (sourceentity instanceof LivingEntity)
				((LivingEntity) sourceentity).setHealth((float) 7);
		}
		if (entity instanceof LivingEntity)
			((LivingEntity) entity).setHealth((float) 5);
		if (sourceentity instanceof PlayerEntity) {
			ItemStack _stktoremove = ((sourceentity instanceof LivingEntity) ? ((LivingEntity) sourceentity).getHeldItemMainhand() : ItemStack.EMPTY);
			((PlayerEntity) sourceentity).inventory.func_234564_a_(p -> _stktoremove.getItem() == p.getItem(), (int) 1,
					((PlayerEntity) sourceentity).container.func_234641_j_());
		}
	}
}
