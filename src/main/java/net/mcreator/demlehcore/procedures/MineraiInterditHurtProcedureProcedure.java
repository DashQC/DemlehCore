package net.mcreator.demlehcore.procedures;

import net.minecraft.util.DamageSource;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.demlehcore.item.NuggetMineraiInterditItem;
import net.mcreator.demlehcore.DemlehcoreMod;

import java.util.Map;

public class MineraiInterditHurtProcedureProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				DemlehcoreMod.LOGGER.warn("Failed to load dependency entity for procedure MineraiInterditHurtProcedure!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemMainhand() : ItemStack.EMPTY)
				.getItem() == NuggetMineraiInterditItem.block
				|| ((entity instanceof LivingEntity) ? ((LivingEntity) entity).getHeldItemOffhand() : ItemStack.EMPTY)
						.getItem() == NuggetMineraiInterditItem.block) {
			entity.attackEntityFrom(DamageSource.GENERIC, (float) 1);
		}
	}
}
