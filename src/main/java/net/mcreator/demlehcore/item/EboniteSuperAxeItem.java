
package net.mcreator.demlehcore.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.SwordItem;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

import net.mcreator.demlehcore.itemgroup.DemlehItemGroup;
import net.mcreator.demlehcore.DemlehcoreModElements;

@DemlehcoreModElements.ModElement.Tag
public class EboniteSuperAxeItem extends DemlehcoreModElements.ModElement {
	@ObjectHolder("demlehcore:ebonite_super_axe")
	public static final Item block = null;

	public EboniteSuperAxeItem(DemlehcoreModElements instance) {
		super(instance, 55);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new SwordItem(new IItemTier() {
			public int getMaxUses() {
				return 5000;
			}

			public float getEfficiency() {
				return 4f;
			}

			public float getAttackDamage() {
				return 12.5f;
			}

			public int getHarvestLevel() {
				return 1;
			}

			public int getEnchantability() {
				return 2;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.EMPTY;
			}
		}, 3, -2.4f, new Item.Properties().group(DemlehItemGroup.tab)) {
		}.setRegistryName("ebonite_super_axe"));
	}
}
