
package net.mcreator.demlehcore.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

import net.mcreator.demlehcore.itemgroup.DemlehItemGroup;
import net.mcreator.demlehcore.DemlehcoreModElements;

@DemlehcoreModElements.ModElement.Tag
public class SouffrePickaxeItem extends DemlehcoreModElements.ModElement {
	@ObjectHolder("demlehcore:souffre_pickaxe")
	public static final Item block = null;

	public SouffrePickaxeItem(DemlehcoreModElements instance) {
		super(instance, 37);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new PickaxeItem(new IItemTier() {
			public int getMaxUses() {
				return 2500;
			}

			public float getEfficiency() {
				return 11f;
			}

			public float getAttackDamage() {
				return 3.5f;
			}

			public int getHarvestLevel() {
				return 5;
			}

			public int getEnchantability() {
				return 2;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.EMPTY;
			}
		}, 1, -3f, new Item.Properties().group(DemlehItemGroup.tab)) {
		}.setRegistryName("souffre_pickaxe"));
	}
}
