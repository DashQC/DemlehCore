
package net.mcreator.demlehcore.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

import net.mcreator.demlehcore.itemgroup.DemlehItemGroup;
import net.mcreator.demlehcore.DemlehcoreModElements;

@DemlehcoreModElements.ModElement.Tag
public class AmazonitepickaxeItem extends DemlehcoreModElements.ModElement {
	@ObjectHolder("demlehcore:amazonitepickaxe")
	public static final Item block = null;

	public AmazonitepickaxeItem(DemlehcoreModElements instance) {
		super(instance, 36);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new PickaxeItem(new IItemTier() {
			public int getMaxUses() {
				return 2000;
			}

			public float getEfficiency() {
				return 9.5f;
			}

			public float getAttackDamage() {
				return 3f;
			}

			public int getHarvestLevel() {
				return 4;
			}

			public int getEnchantability() {
				return 2;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.EMPTY;
			}
		}, 1, -3f, new Item.Properties().group(DemlehItemGroup.tab)) {
		}.setRegistryName("amazonitepickaxe"));
	}
}
