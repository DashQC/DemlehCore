
package net.mcreator.demlehcore.itemgroup;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;

import net.mcreator.demlehcore.item.EboniteItem;
import net.mcreator.demlehcore.DemlehcoreModElements;

@DemlehcoreModElements.ModElement.Tag
public class DemlehItemGroup extends DemlehcoreModElements.ModElement {
	public DemlehItemGroup(DemlehcoreModElements instance) {
		super(instance, 149);
	}

	@Override
	public void initElements() {
		tab = new ItemGroup("tabdemleh") {
			@OnlyIn(Dist.CLIENT)
			@Override
			public ItemStack createIcon() {
				return new ItemStack(EboniteItem.block);
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}

	public static ItemGroup tab;
}
