
package net.mcreator.demlehcore.fuel;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.furnace.FurnaceFuelBurnTimeEvent;

import net.mcreator.demlehcore.item.MagmaNuggetItem;

@Mod.EventBusSubscriber
public class MagmaFuelFuel {
	@SubscribeEvent
	public static void furnaceFuelBurnTimeEvent(FurnaceFuelBurnTimeEvent event) {
		if (event.getItemStack().getItem() == MagmaNuggetItem.block)
			event.setBurnTime(18000);
	}
}
